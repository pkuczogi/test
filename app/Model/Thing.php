<?php

App::uses('AppModel', 'Model');

/**
 * Thing Model
 *
 */
class Thing extends AppModel {

	public $actsAs = array(
		'Upload.Upload' => array(
			'photo' => array(
				'fields' => array(
					'dir' => 'photo_dir'
				),
					'thumbnailSizes' => array(
						'thumb1' => '200x200',
						'thumb2' => '300x200'
					)
			)
		)
	);
	
	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
			//'message' => 'Your custom message here',
			//'allowEmpty' => false,
			//'required' => false,
			//'last' => false, // Stop validation after this rule
			//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),

	);

}
