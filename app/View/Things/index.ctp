<div class="things index">
	<h2><?php echo __('Things'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('photo'); ?></th>
			<th><?php echo $this->Paginator->sort('photo_dir'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($things as $thing): ?>
	<tr>
		<td><?php echo h($thing['Thing']['id']); ?>&nbsp;</td>
		<td><?php echo h($thing['Thing']['name']); ?>&nbsp;</td>
		<td><?php echo h($thing['Thing']['photo']); ?>&nbsp;</td>
		<td><?php echo h($thing['Thing']['photo_dir']); ?>&nbsp;</td>
		<td><?php echo h($thing['Thing']['created']); ?>&nbsp;</td>
		<td><?php echo h($thing['Thing']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $thing['Thing']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $thing['Thing']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $thing['Thing']['id']), null, __('Are you sure you want to delete # %s?', $thing['Thing']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Thing'), array('action' => 'add')); ?></li>
	</ul>
</div>
