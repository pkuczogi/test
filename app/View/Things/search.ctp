<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.js"></script>

<script>

$(document).ready(function(){
	
	resultsDiv();
	
	$(window).resize(function(){
		resultsDiv();
	});

});


function resultsDiv(){
	
	if($(window).width() < 960){
	
		$("#results").css('width', 320);
		updateImg('thumb1_', 'thumb2_');
	
	}else{
		
		$("#results").css('width', 1000);
		updateImg('thumb2_', 'thumb1_');
	}

}

function updateImg(p1, p2) {
$('.picture').find('img').attr('src', function(i, val ) {
    return val.replace(p1,p2);
});
}
</script>

<div style="width: 320px; margin-left: auto; margin-right: auto;">
<?php
	echo $this->Form->create('searchBox', array('inputDefaults' => array('div' => false, 'label' => false, 'style' => 'width:300px; align: center; padding: 10px;')));
	echo $this->Form->input('searchTerm', array('type' => 'text'));
	echo $this->Form->end('Keres'); 
?>
</div>

<div id="results" style="width: 1000px; margin-left: auto; margin-right: auto;">

	<?php foreach ($things as $thing): ?>
<div class="picture" style="font-size:15px; text-align:center; float: left; padding: 10px;"><img src="<?php echo $this->webroot . 'files/thing/photo/' . ($thing['Thing']['photo_dir']) . '/thumb1_' .  ($thing['Thing']['photo']); ?>"><br><?php echo ($thing['Thing']['name']); ?></div>
<?php endforeach; ?>

</div>