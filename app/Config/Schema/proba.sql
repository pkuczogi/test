CREATE TABLE IF NOT EXISTS `things` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `photo_dir` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

INSERT INTO `things` (`id`, `name`, `photo`, `photo_dir`, `created`, `modified`) VALUES
(1, 'Penguins', 'Penguins.jpg', '1', '2014-03-13 09:35:50', '2014-03-13 09:36:29'),
(2, 'Koala', 'Koala.jpg', '2', '2014-03-13 09:36:52', '2014-03-13 09:36:52'),
(3, 'Hortenzia', 'Hydrangeas.jpg', '3', '2014-03-13 09:37:09', '2014-03-13 09:37:09');

